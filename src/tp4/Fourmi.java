package tp4;

public class Fourmi {
	// Attrributs
	protected int x; // x et y sont les coordonn�es de notre fourmi
	protected int y;
	protected int orientation; // orientation est l'orientation de la fourmi (0,90,180 ou 270)

	public Fourmi(int x, int y) {
		orientation = 0;
		this.x = x;
		this.y = y;
	}

	public int getOrientation() {  //On r�cup�re l'orientation
		return orientation;
	}
	
	public void tournerAGauche() {   //On fait tourner � gauche notre fourmi
		orientation = orientation - 90;
		if(orientation<0) {
			orientation = 270;
		}
	}

	public void tournerADroite() {    //On fait tourner � droite notre fourmi
		orientation = orientation + 90;
		if(orientation>270) {
			orientation = 0 ;
		}
	}

	public void avancer() {     //On fait avancer notre fourmi
		if(orientation == 0) {
			x = x-1;
		}
		else if(orientation == 90){
			y = y+1;
		}
		else if(orientation == 180){
			x = x+1;
		}
		else if(orientation == 270){
			y = y-1;
		}
	}
}

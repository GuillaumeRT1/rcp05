package tp4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tp3.UneApplicationGraphique;

public class LaFourmiDeLangton extends JFrame implements ActionListener {
	private Container panneau;
	private JButton monBouton;
	private JLabel monLabel;
	private int dimension = 10;
	private JTextField plateau[][];
	private Fourmi f;

	public LaFourmiDeLangton() {
		// Cr�ation de la fen�tre
		super("La Fourmi De Langton");
		setSize(500, 500);
		setLocation(300, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new BorderLayout(3, 1));

		// Cr�ation des objets � utiliser
		monLabel = new JLabel("Nombre de tours : 0");
		monBouton = new JButton("Suivant svp");
		JPanel p = creerPlateau();

		// Ajout au panneau
		panneau.add(monLabel, BorderLayout.NORTH);
		panneau.add(monBouton, BorderLayout.SOUTH);
		panneau.add(p);

		monBouton.addActionListener(this);

		setVisible(true);
	}

	public JPanel creerPlateau() {    //On cr�e le plateau de jeu
		JPanel p = new JPanel();
		plateau = new JTextField[dimension][dimension];
		p.setLayout(new GridLayout(dimension, dimension));
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				JTextField s = new JTextField();
				plateau[i][j] = s;
				p.add(s);
			}
		}
		return p;
	}

	public void initFourmi() {
		f = new Fourmi(dimension / 2, dimension / 2);
		plateau[f.x][f.y].setText("      " + f.getOrientation());
	}

	public static void main(String[] args) {
		LaFourmiDeLangton n = new LaFourmiDeLangton();
		n.initFourmi();
		}

	public void next() {
		f.tournerADroite();
		plateau[f.x][f.y].setText(" " );
		f.avancer();
		plateau[f.x][f.y].setText(" " + f.getOrientation());   // Ici je montre que mes m�thodes marchent
		//f.tournerAGauche();     //A decommenter si l'on souhaite tester l'autre.
		//plateau[f.x][f.y].setText(" " );
		//f.avancer();
		//plateau[f.x][f.y].setText(" " + f.getOrientation());   // Car les test de couleurs ne fonctionnaient pas (il sautait des cases). 
	}

	public void actionPerformed(ActionEvent e) {   //Ici c'est pour que le boutton puisse avoir un effetavec les lignes du next
		System.out.println("test");
		next();
	}
}

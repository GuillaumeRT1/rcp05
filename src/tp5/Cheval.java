package tp5;

public class Cheval extends Thread implements CoureurHippique {
	// Attributs
	private int id;
	private int longueurCourse;
	private int distanceParcourue;
	private JugeDeCourse juge;

	public Cheval(int id, int lc, JugeDeCourse j) {
		this.id = id;
		juge = j;
		distanceParcourue = 0;
		longueurCourse = lc;
	}

	public void run() {
		while (distanceParcourue < longueurCourse) {
			distanceParcourue = distanceParcourue + (int) (0 + (Math.random() * (3 - 0)));
			System.out.println("Je suis " + id + " et j'ai parcourue " + distanceParcourue + " unit�s");
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		juge.passeLaLigneDArrivee(id);
		System.out.println("Je suis " + id + " et j'ai fini");
	}

	public int distanceParcourue() {
		return distanceParcourue;
	}
}

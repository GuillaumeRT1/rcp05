package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import tp3.UneApplicationGraphiqueActive;

public class Tierce extends JFrame implements ActionListener, JugeDeCourse {
	private Container panneau;
	private JButton monBouton;
	private JTextField monChampTexte;
	private JLabel monLabel;
	private Cheval ch[];
	private Cheval c;

	public Tierce() {
		// Cr�ation de la fen�tre
		super("Tierce Gagnant");
		setSize(300, 250);
		setLocation(20, 20);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new BorderLayout());

		// Cr�ation des objets � utiliser
		monLabel = new JLabel("R�sultat :");
		monBouton = new JButton("Go Pokemon");

		// Ajout au panneau
		panneau.add(monLabel, BorderLayout.NORTH);
		panneau.add(monBouton, BorderLayout.SOUTH);

		setVisible(true);
		ch = new Cheval[7];
		for (int i = 1; i < 7; i = i + 1) {
			c = new Cheval(i, 50, this);
			ch[i] = c;
		}
		monBouton.addActionListener(this);

	}

	public static void main(String[] args) {
		Tierce t = new Tierce();
	}

	public void traiterMonBouton() {
		System.out.println("Click");
		for (int i = 1; i < 7; i = i + 1) {
			ch[i].start();
		}
	}

	public void actionPerformed(ActionEvent e) {
		traiterMonBouton();
	}

	public synchronized void passeLaLigneDArrivee(int id) {
		monLabel.setText(monLabel.getText()+" "+id );
	}
}

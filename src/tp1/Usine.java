package tp1;

import java.util.Vector;

public class Usine {
	//Attributs
	private Vector<Stuart> listeStuart;
	private String cloche = "Driiiiiing!";
	
	//Constructeur
	public Usine() {
		listeStuart = new Vector<Stuart>();
	}
	
	//Methodes
	public void recruter() { //Ex2.1
		Stuart stuart;
		stuart = new Stuart();
		listeStuart.addElement(stuart);
	}
	
	public void afficherListeEmployer() {
		System.out.println("Voici la liste des employees " + listeStuart);
	}
	public void sonnerLaCloche() {
		System.out.println(cloche);
		int i =0;
		while(i<listeStuart.size()) {
			listeStuart.elementAt(i).sePresenter();
			listeStuart.elementAt(i).travailler();
			listeStuart.elementAt(i).sePresenter();
			i=i+1;
		}
		
	}
}

package tp3;

public class VerserPleinDArgent implements Runnable {
	// Attribut
	private Compte compte;

	// Constructeur
	public VerserPleinDArgent(Compte compte) {
		this.compte = compte;
	}

	public void run() {
		for (int i = 0; i < 100; i++) {
			compte.verser(2);
			compte.getSolde();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}

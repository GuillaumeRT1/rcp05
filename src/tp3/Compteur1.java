package tp3;

public class Compteur1 extends Thread {

	private String nom;

	public Compteur1(String nom) {
		this.nom = nom;
	}

	public void run() {
		for (int i = 1; i < 1000; i++)
			System.out.print(i + " " + nom);
	}

	public static void main(String args[]) throws InterruptedException {
		Compteur1 t1, t2, t3;
		t1 = new Compteur1("Hello ");
		t2 = new Compteur1("World ");
		t3 = new Compteur1("and Everybody ");

		t1.start();
		t2.start();
		t3.start();
		Thread.sleep(100); // Ex 1.2 en ajoutant un sleep on permet au prgm de faire les 1000 affichages
							// avant d'arriver � la sortie du main.
		// Ex 1.3 L'affichage reste al�atoire.
		System.out.println("Fin du programme");

		System.exit(0); // Ex 1.1 l'affichage du compteur est al�atoire de plus il n'atteint pas les
						// 1000 car la ligne System.exit(0);
						// arr�te le main avant la fin donc on ne pourras pas atteindre les 1000
						// affichages.
	}

}

package tp3;

public class Compteur2 extends Thread {

	private String nom;

	public Compteur2(String nom) {
		this.nom = nom;
	}

	public void run() {
		for (int i = 1; i < 1000; i++)
			System.out.print(i + " " + nom);
	}

	public static void main(String args[]) {
		Compteur2 t1, t2, t3;
		t1 = new Compteur2("Hello ");
		t2 = new Compteur2("World ");
		t3 = new Compteur2("and Everybody ");

		t1.start();
		t2.start();
		t3.start();

		try {
			t1.join(); // quand il y a uniquement join() pour t1, le prgm attend juste que t1 soir
						// ex�cuter pour poursuivre, on doit peut avoirs
						// la phrase Fin du Programme � n'importe quelle moment dans l'�xecution de t2
						// et t3 mais forcc�ment apr�s t1
			t2.join();
			t3.join();  // A partir de maintenant la phrase Fin du Programme est affich� apr�s ex�cution de t1, t2, t3.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Fin du programme");

		System.exit(0);

	}

}

package tp3;

public class Banque {
	public static void main(String[] args) {
		// Cr�ation du compte c1 pur ex 3.2
		Compte c1;
		c1 = new Compte();

		// Affichage solde -> Versement -> Affichage solde
		c1.getSolde();
		c1.verser(2);
		c1.getSolde();

		// Exercice 3.3
		Compte c2 = new Compte();
		VerserPleinDArgent v1;
		v1 = new VerserPleinDArgent(c2);
		Thread th2 = new Thread(v1);
		th2.start();

		// Exercice 3.4
		VerserPleinDArgent v2;
		v2 = new VerserPleinDArgent(c2);
		Thread th3 = new Thread(v2);
		th3.start();
		// Nous n'arrivons pas � 400 euros. Ex 3.6 solde sera affich� deusx fois � la
		// m�me valeur en effet solde = 2 apr�s th2 et solde = 2 lorsque th1 reprend on
		// aura deux affichage de la m�me somme .
	}
}

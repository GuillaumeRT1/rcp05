package tp2;

public class Appareilelectromenager extends Produit implements VenteUnite,Soldes {
	// Attributs
	private int codeBarre;
	private float prixVenteInit;

	// Constructeur
	public Appareilelectromenager(String nom, float prixAchat, float prixVente, int quantite, int codeBarre) {
		super(nom, prixAchat, prixVente, quantite);
	}

	// M�thodes
	public void augmenterStock(int quantite) {
		this.quantite = this.quantite + quantite;
		;
	}

	public void diminuerStock(int quantite) {
		this.quantite = this.quantite - quantite;
	}

	public void venteAUnite(int quantite) {
		if (quantite <= this.quantite) {
			diminuerStock(quantite);
			Magasin.capital = (int) (Magasin.capital + (quantite * prixVente));
			System.out.println("Achat effectu�, merci!");
		} else {
			System.out.println("D�sol� le stock n'est plus suffisant pour cet achat !");
		}
	}

	public void solde(float s) { // m�thode pour l'application des soldes
		System.out.println("D�but des soldes de -" + s + " %");
		s = (s / 100) * prixVente;
		prixVenteInit = prixVente;
		prixVente = prixVente - s;
	}

	public void retourNormal() { // m�thode pour le retour du prix � la normal
		prixVente = prixVenteInit;
		System.out.println("Fin des soldes !");
	}
}

package tp2;

import java.util.Vector;

public class Magasin { // Je cr�e la classe magasin dans le but de pouvoir augmenter le capital �
						// chaque vente.
	// Attributs
	public static int capital;
	private Vector<Produit> listeProduit;

	// Constructeur
	public Magasin() {
		listeProduit = new Vector<Produit>();
		capital = 1000;
	}

	public void afficherListeProduit() {
		System.out.println("Voici la liste des produits du magasin " + listeProduit);
	}
}

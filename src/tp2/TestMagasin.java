package tp2;

public class TestMagasin {

	public static void main(String[] args) {
		// On cr�e notre magasin en premier avant les produits
		Magasin mag1 = new Magasin();
		// Cr�ation et instanciation des objets � cr�er dans l'exercice 1
		Appareilelectromenager television;
		television = new Appareilelectromenager("Television", 2000, 3500, 4, 01020304);

		Fruitalapiece ananas;
		ananas = new Fruitalapiece("Ananas", 1, 3, 30, "R�union");

		Fruitenvrac bananes;
		bananes = new Fruitenvrac("Bananes", 2, 5, (float) 2.8, "Afrique du Sud");

		// Pr�sentation de nos produits
		System.out.println("Voici la description des produits avant changement");
		television.seDecrire();
		ananas.seDecrire();
		bananes.seDecrire();

		// Gestion de l'augmentation et de diminution des stocks
		television.augmenterStock(2);
		bananes.diminuerStock((float) 1.5);

		// Pr�sentation de nos produits 2
		System.out.println("Voici la description des produits avant changement");
		television.seDecrire();
		ananas.seDecrire();
		bananes.seDecrire();

		// Exercice 2
		// Test de vente d'ananas
		System.out.println(Magasin.capital);
		ananas.venteAUnite(3);
		ananas.seDecrire();
		System.out.println(Magasin.capital);

		// Test de vente de television
		television.venteAUnite(4);
		television.seDecrire();
		System.out.println(Magasin.capital);

		// Test de vente de bananes et dans le cas �ch�ant
		bananes.ventePoids((float) 2.6);
		bananes.seDecrire();
		System.out.println(Magasin.capital);
		bananes.ventePoids((float) 1.3);
		bananes.seDecrire();
		System.out.println(Magasin.capital);

		// Test de Soldes pour les bananes (fruit au poids)
		bananes.solde(50);
		bananes.seDecrire();
		bananes.retourNormal();
		bananes.seDecrire();
		
		//Test pour television pareil
		television.solde(60);
		television.seDecrire();
		television.venteAUnite(1);
		television.seDecrire();
		System.out.println(Magasin.capital);
		television.retourNormal();
		television.seDecrire();
	}

}

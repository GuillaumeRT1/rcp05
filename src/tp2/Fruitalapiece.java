package tp2;

public class Fruitalapiece extends Fruit implements VenteUnite {

	// Constructeur
	public Fruitalapiece(String nom, float prixAchat, float prixVente, float quantite, String paysOrigine) {
		super(nom, prixAchat, prixVente, quantite, paysOrigine);
	}

	public void venteAUnite(int quantite) {
		if (quantite <= this.quantite) {
			super.diminuerStock(quantite);
			Magasin.capital = (int) (Magasin.capital + (quantite * prixVente));
			System.out.println("Achat effectu�, merci!");
		} else {
			System.out.println("D�sol� le stock n'est plus suffisant pour cet achat !");
		}
	}

}

package tp2;

public abstract class Fruit extends Produit {
	// Attributs
	protected static String paysOrigine;

	public Fruit(String nom, float prixAchat, float prixVente, float quantite, String paysOrigine) {
		super(nom, prixAchat, prixVente, quantite);
	}

	// M�thodes
	public void augmenterStock(float quantite) {
		this.quantite = this.quantite + quantite;
		;
	}

	public void diminuerStock(float quantite) {
		this.quantite = this.quantite - quantite;
	}
}

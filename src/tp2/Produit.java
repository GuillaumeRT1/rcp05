package tp2;

public abstract class Produit {
	// Attributs
	public String nom;
	public float prixAchat;
	public float prixVente;
	public float quantite;

	// Constructeur
	public Produit(String nom, float prixAchat, float prixVente, float quantite) {
		this.nom = nom;
		this.prixAchat = prixAchat;
		this.prixVente = prixVente;
		this.quantite = quantite;
	}

	// M�thodes
	public void seDecrire() {
		System.out.println("Je suis " + nom + ", je co�te " + prixVente + " euros, et je suis disponible au nombre de "
				+ quantite);
	}
}

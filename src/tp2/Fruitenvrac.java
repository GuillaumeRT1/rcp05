package tp2;

public class Fruitenvrac extends Fruit implements VentePoids, Soldes {
	// Attributs
	private float prixVenteInit;

	// Constructeur
	public Fruitenvrac(String nom, float prixAchat, float prixVente, float quantite, String paysOrigine) {
		super(nom, prixAchat, prixVente, quantite, paysOrigine);
	}

	// M�thodes
	public void ventePoids(float quantite) {
		if (quantite <= this.quantite) {
			diminuerStock(quantite);
			Magasin.capital = (int) (Magasin.capital + (quantite * prixVente));
			System.out.println("Achat effectu�, merci!");
		} else {
			System.out.println("D�sol� le stock n'est plus suffisant pour cet achat !");
		}
	}

	public void solde(float s) { //m�thodes pour l'application des soldes
		System.out.println("D�but des soldes de -" + s + " %");
		s = (s / 100) * prixVente;
		prixVenteInit = prixVente;
		prixVente = prixVente - s;
	}

	public void retourNormal() { //m�thode pour la retour � la normal apr�s les soldes
		prixVente = prixVenteInit;
		System.out.println("Fin des soldes !");
	}

}
